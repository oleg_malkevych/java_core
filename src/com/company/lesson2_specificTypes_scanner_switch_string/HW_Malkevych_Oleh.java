package com.company.lesson2_specificTypes_scanner_switch_string;

import java.util.Scanner;

public class HW_Malkevych_Oleh {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        double firstNumber = 0;
        double secondNumber = 0;
        boolean isZero = true;

        System.out.println("Калькулятор");

        System.out.print("\nInput first number : ");
        if (input.hasNextDouble()) {
            firstNumber = input.nextDouble();
        }else {
            input.nextLine();
        }

        System.out.print("\nInput sign : ");
        String sign = input.nextLine();

        System.out.print("\nInput second number : ");
        if (input.hasNextDouble()) {
            secondNumber = input.nextDouble();
            isZero = false;

        }else {
            input.nextLine();
        }

        double result;
        result = switch (sign){
            case "+" -> firstNumber + secondNumber;
            case "-" -> firstNumber - secondNumber;
            case "*" -> firstNumber * secondNumber;
            case "/" -> firstNumber / secondNumber;
            default -> throw new IllegalStateException("Unexpected value: " + sign);
        };

        if (secondNumber == 0 && !isZero ) {
            System.out.println("We can't  / on 0");
        } else {
            System.out.println("\nResult : " + result);
        }

    }
}
