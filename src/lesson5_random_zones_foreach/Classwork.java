package lesson5_random_zones_foreach;

import java.util.Random;

public class Classwork {
    public static void main(String[] args) {
        int [] array = new int[10];
        Random random = new Random();

        int min = 50;
        int max = 150;
        int diff = max - min;

        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(diff) + min;
            System.out.println(array[i]);
        }
    }
}
