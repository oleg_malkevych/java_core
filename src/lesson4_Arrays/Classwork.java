package lesson4_Arrays;

import java.util.Scanner;

public class Classwork {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);



        System.out.print("Введіть розмір масиву : ");
        int sizeOfArray = input.nextInt();

        int[] array = new int[sizeOfArray];

        for (int i = 0; i < array.length ; i++) {
            array[i] = input.nextInt();
        }
        for (int j : array) {
            System.out.print(j + " ");
        }

    }
}
